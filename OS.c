// OS.c

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "../inc/tm4c123gh6pm.h"
#include "OS.h"
#include "SysTickInts.h"
#include "PLL.h"

#include "ST7735.h"

#define PLL_FREQUENCY 80000000
#define MAX_ST_PERIOD 16777215	//2^24 - 1

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
void (*PeriodicTask)(void);   // user function

void SysTick_Init(uint32_t period);
void SysTick_InitPriority(uint32_t period, uint32_t priority);
uint32_t SysTick_Read(void);
void SysTick_Handler(void);

uint32_t stCount, stRepeats;

void OS_AddPeriodicThread(void(*task)(void), uint32_t ms, uint32_t priority){
	PeriodicTask = task;          // user function
  SysTick_InitPriority(PLL_FREQUENCY/1000 * ms, priority);        // initialize SysTick timer; "period" milliseconds
     
}

// Ends the current periodic thread
void OS_StopPeriodicThread(void){
	NVIC_ST_CTRL_R = 0;
	NVIC_ST_CURRENT_R = 0;
}

void OS_ClearPeriodicTime(){
	long sr = StartCritical();
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
	EndCritical(sr);
}

uint32_t OS_ReadPeriodicTime(void){
  return (SysTick_Read());
}

// SysTick Init with Priority
void SysTick_InitPriority(uint32_t period, uint32_t priority){long sr;
  sr = StartCritical();
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
	
	// Max period is 2^24 - 1. If larger period needed, divide by two and multiply the stRepeat
	stRepeats = 1;
	stCount = 0;
	while (period > MAX_ST_PERIOD){
		period >>= 1;
		stRepeats <<= 1;
	}
	NVIC_ST_RELOAD_R = period;
  NVIC_ST_CURRENT_R = 0;      // any write to current clears it
	priority = (priority & 0x7) << 29;
	NVIC_SYS_PRI3_R = (NVIC_SYS_PRI3_R&0x00FFFFFF)| priority;
                              // enable SysTick with core clock and interrupts
  NVIC_ST_CTRL_R = 0x07;
  EndCritical(sr);
}

uint32_t SysTick_Read(){long sr;
	uint32_t ret;
  sr = StartCritical();
  NVIC_ST_CTRL_R = 0;         // disable SysTick during setup
	ret = NVIC_ST_RELOAD_R;
	NVIC_ST_CTRL_R = 0x07;		//enable
  EndCritical(sr);
	return ret;
}

// Interrupt service routine
void SysTick_Handler(void){
	
	if (++stCount % stRepeats == 0)
		(*PeriodicTask)();                // execute user task
}
