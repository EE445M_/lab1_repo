//OS.h

// Creates a periodic thread
void OS_AddPeriodicThread(void(*task)(void), uint32_t period, uint32_t priority);

// Resets the OS SysTick timer
void OS_ClearPeriodicTime(void);

// Returns the current SysTick time
uint32_t OS_ReadPeriodicTime(void);

// Stop the periodic thread
void OS_StopPeriodicThread(void);
