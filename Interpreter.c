// Interpreter.c
// This file will provide the main method and interpreter

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "Interpreter.h"
#include "ST7735.h"
#include "PLL.h"
#include "UART.h"
#include "ADC.h"
#include "OS.h"
#include "../inc/tm4c123gh6pm.h"

#define MAX_INPUT_LENGTH 50
#define MAX_ARGS 8
#define NO_NUM ST7735_NO_NUM

char userCommand[MAX_INPUT_LENGTH];
char userArgs[MAX_ARGS][MAX_INPUT_LENGTH];
int numArgs;
int numSamples;
uint32_t sampleBuffer[100];
uint8_t bottomLine = 0;

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts

int main(void){
	PLL_Init(Bus80MHz);
	ST7735_InitScreen();
	UART_Init();
	UART_NewLine();
	
	uint8_t result = 0;	// holds result of attempting to execute command
		
	while(1){
		Interpreter_ParseCommand();
		if (strcmp(userCommand, "write") == 0){
			result = Interpreter_WriteMessage();
		} else if (strcmp(userCommand, "time") == 0){
			result = Interpreter_SetTimer();
		} else if (strcmp(userCommand, "stop") == 0){
			result = Interpreter_StopPeriodic();
		} else if (strcmp(userCommand, "samp") == 0){
			result = Interpreter_SampleADC();
		} else if (strcmp(userCommand, "collect") == 0){
			result = Interpreter_CollectADC();
		} else if (strcmp(userCommand, "print") == 0){
			result = Interpreter_PrintSamples();
		} 
		
		if (result == 0){
			UART_OutStringLn("Error parsing command.");
		}
	}
}

// Reads input from UART and parses into command and arguments.
// Stores results in global variables.
void Interpreter_ParseCommand(void){
	char userInput[MAX_INPUT_LENGTH];
	UART_OutString("Enter a command: ");
	UART_InString(userInput, MAX_INPUT_LENGTH);
	UART_NewLine();
	
	numArgs = 0;
	char* strPtr =  userInput;
	char* endPtr = strchr(userInput, ' ');
	
	if (endPtr != NULL) {		
		*endPtr = '\0';
		strcpy(userCommand, strPtr);
		
		while (endPtr && *(endPtr+1) != '\0'){
			strPtr = endPtr + 1;
			if (*strPtr == '\"'){
				strPtr++;
				endPtr = strchr(strPtr, '\"');
				if (*endPtr != '"'){
					return;
				}
			} else {
				endPtr = strchr(strPtr, ' ');
			}
			if (endPtr) 
				*endPtr = '\0';
			if (*strPtr != '\0') 
				strcpy(userArgs[numArgs++], strPtr);
		}
	} else {
		strcpy(userCommand, userInput);
	}
}

// Displays an action performed on the top screen of the lcd
void Interpreter_ListAction(char* str, int32_t value){	
	static int line = 0;
	if (line == 7){
		ST7735_ClearScreen(ST7735_TOP_SCREEN);
		line = 0;
	}
	ST7735_Message(ST7735_TOP_SCREEN, line++, str, value);
}

// Writes a message to the top of the bottom screen
uint8_t Interpreter_WriteMessage(void){
	if (numArgs == 1){
		ST7735_Message(ST7735_BOTTOM_SCREEN, 0, userArgs[0], 0);
		Interpreter_ListAction("Message written.", NO_NUM);
		return 1;
	}
		return 0;
}

// Starts a timer that periodically outputs a message
uint8_t Interpreter_SetTimer(void){
	if (numArgs == 1){
		uint32_t period = strtol(userArgs[0], NULL, 10);
		if (period >= 50){	// SysTick cannot repeat too quickly or else system will jam
			OS_AddPeriodicThread(&Interpreter_PeriodicTime, period, 1);
			Interpreter_ListAction("Starting timer.", NO_NUM);
			return 1;
		}
	}
	return 0;
}

// Stops the timer
uint8_t Interpreter_StopPeriodic(void){
	if (numArgs == 0){
		OS_StopPeriodicThread();
		Interpreter_ListAction("Stopping.", NO_NUM);
		return 1;
	}
	return 0;
}

// Samples the ADC and prints to the top screen
// samp
// samp [channel]
// samp [channel] [frequency]
uint8_t Interpreter_SampleADC(void){
	if (numArgs == 0){
		Interpreter_ListAction("ADC sample: ", ADC_InChan(0));
		return 1;
	} 
	int8_t channel;
	if (*userArgs[0] != 'p'){
		channel = strtol(userArgs[0], NULL, 10);
	} else {
		channel = ADC_GetChannel(userArgs[0]);
		if (channel == -1) 
			return 0;
	}
	
	if (numArgs == 1){
		Interpreter_ListAction("ADC sample: ", ADC_InChan(channel));
	} else if (numArgs == 2){
		uint32_t period = strtol(userArgs[1], NULL, 10);
		if (period <= 50) 
			return 0;
		ADC_Open(channel);
		Interpreter_ListAction("Begin sampling.", NO_NUM);
		OS_AddPeriodicThread(&Interpreter_PeriodicADC, period, 6);
	} else {
		return 0;
	}
	return 1;
}

// Collects samples from the ADC and stores results in a buffer
// collect [channel] [frequency] [num samples]
uint8_t Interpreter_CollectADC(void){
	if (numArgs == 3){
		int8_t channel;
		if (*userArgs[0] != 'p'){
			channel = strtol(userArgs[0], NULL, 10);
		} else {
			channel = ADC_GetChannel(userArgs[0]);
			if (channel == -1)
				return 0;
		}
		uint32_t freq = strtol(userArgs[1], NULL, 10);
		numSamples = strtol(userArgs[2], NULL, 10);
		ADC_Collect(channel, freq, sampleBuffer, numSamples);
		Interpreter_ListAction("Collecting samples:", NO_NUM);
		Interpreter_ListAction("  channel: ", channel);
		Interpreter_ListAction("  freq:    ", freq);
		Interpreter_ListAction("  samples: ", numSamples);
		return 1;
	}
	return 0;
}

// Prints out the buffered samples on the bottom screen
uint8_t Interpreter_PrintSamples(void){
	if (numArgs == 0){
		if (ADC_SamplesReady()){
			Interpreter_ListAction("Printing samples.", NO_NUM);
			ST7735_ClearScreen(ST7735_BOTTOM_SCREEN);
			ST7735_Message(ST7735_BOTTOM_SCREEN, 0, "Data samples:", NO_NUM);
			int row = 9;
			int col = 0;
			for (uint8_t i = 0; i < numSamples; i++){
				ST7735_SetCursor(col, row);
				ST7735_OutUDec(sampleBuffer[i]);
				row++;
				if (row > 15){
					row = 9;
					col += 5;
				}
			}
		} else {
			Interpreter_ListAction("Samples not ready.", NO_NUM);
		}
		return 1;
	}
	return 0;
}


//////////////////////
// Periodic methods //
//////////////////////

// Writes "Time." to the LCD screen
void Interpreter_PeriodicTime(void){
	Interpreter_ListAction("Time.", NO_NUM);
}
	
// Samples the ADC and prints result to screen
void Interpreter_PeriodicADC(void){
	Interpreter_ListAction("ADC sample: ", ADC_In());
}


