#include <stdint.h>
#include "../inc/tm4c123gh6pm.h"
#include "ADC.h"
#include "ST7735.h"
#include <string.h>

#define PLL_FREQUENCY 80000000
#define MAX_ADC_PERIOD 65535

uint32_t *adcBuffer;
uint8_t bufferReady = 0;
uint32_t bufferPtr;
uint32_t bufferSize;
uint32_t adcCount, adcRepeats;

void DisableInterrupts(void);
void EnableInterrupts(void);

void ADC_Collect(uint32_t channelNum, uint32_t freq, uint32_t *buffer, uint32_t numSamples){
	bufferReady = 0;
	uint32_t period = PLL_FREQUENCY / freq;
	adcBuffer = buffer;
	bufferPtr = 0;
	bufferSize = numSamples;
	ADC_OpenPeriodic(channelNum, period);
}

// Returns the status of the sample collection
uint8_t ADC_SamplesReady(void){
	return bufferReady;
}

void ADC_Open(uint8_t channelNum){
	ADC_OpenPeriodic(channelNum, 0);
}

void ADC_OpenPeriodic(uint8_t channelNum, uint32_t period){
  volatile uint32_t delay;
  // **** GPIO pin initialization ****
  switch(channelNum){             // 1) activate clock
    case 0:
    case 1:
    case 2:
    case 3:
    case 8:
    case 9:                       //    these are on GPIO_PORTE
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R4; break;
    case 4:
    case 5:
    case 6:
    case 7:                       //    these are on GPIO_PORTD
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R3; break;
    case 10:
    case 11:                      //    these are on GPIO_PORTB
      SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1; break;
    default: return;              //    0 to 11 are valid channels on the LM4F120
  }
  delay = SYSCTL_RCGCGPIO_R;      // 2) allow time for clock to stabilize
  delay = SYSCTL_RCGCGPIO_R;
  switch(channelNum){
    case 0:                       //      Ain0 is on PE3
      GPIO_PORTE_DIR_R &= ~0x08;  // 3.0) make PE3 input
      GPIO_PORTE_AFSEL_R |= 0x08; // 4.0) enable alternate function on PE3
      GPIO_PORTE_DEN_R &= ~0x08;  // 5.0) disable digital I/O on PE3
      GPIO_PORTE_AMSEL_R |= 0x08; // 6.0) enable analog functionality on PE3
      break;
    case 1:                       //      Ain1 is on PE2
      GPIO_PORTE_DIR_R &= ~0x04;  // 3.1) make PE2 input
      GPIO_PORTE_AFSEL_R |= 0x04; // 4.1) enable alternate function on PE2
      GPIO_PORTE_DEN_R &= ~0x04;  // 5.1) disable digital I/O on PE2
      GPIO_PORTE_AMSEL_R |= 0x04; // 6.1) enable analog functionality on PE2
      break;
    case 2:                       //      Ain2 is on PE1
      GPIO_PORTE_DIR_R &= ~0x02;  // 3.2) make PE1 input
      GPIO_PORTE_AFSEL_R |= 0x02; // 4.2) enable alternate function on PE1
      GPIO_PORTE_DEN_R &= ~0x02;  // 5.2) disable digital I/O on PE1
      GPIO_PORTE_AMSEL_R |= 0x02; // 6.2) enable analog functionality on PE1
      break;
    case 3:                       //      Ain3 is on PE0
      GPIO_PORTE_DIR_R &= ~0x01;  // 3.3) make PE0 input
      GPIO_PORTE_AFSEL_R |= 0x01; // 4.3) enable alternate function on PE0
      GPIO_PORTE_DEN_R &= ~0x01;  // 5.3) disable digital I/O on PE0
      GPIO_PORTE_AMSEL_R |= 0x01; // 6.3) enable analog functionality on PE0
      break;
    case 4:                       //      Ain4 is on PD3
      GPIO_PORTD_DIR_R &= ~0x08;  // 3.4) make PD3 input
      GPIO_PORTD_AFSEL_R |= 0x08; // 4.4) enable alternate function on PD3
      GPIO_PORTD_DEN_R &= ~0x08;  // 5.4) disable digital I/O on PD3
      GPIO_PORTD_AMSEL_R |= 0x08; // 6.4) enable analog functionality on PD3
      break;
    case 5:                       //      Ain5 is on PD2
      GPIO_PORTD_DIR_R &= ~0x04;  // 3.5) make PD2 input
      GPIO_PORTD_AFSEL_R |= 0x04; // 4.5) enable alternate function on PD2
      GPIO_PORTD_DEN_R &= ~0x04;  // 5.5) disable digital I/O on PD2
      GPIO_PORTD_AMSEL_R |= 0x04; // 6.5) enable analog functionality on PD2
      break;
    case 6:                       //      Ain6 is on PD1
      GPIO_PORTD_DIR_R &= ~0x02;  // 3.6) make PD1 input
      GPIO_PORTD_AFSEL_R |= 0x02; // 4.6) enable alternate function on PD1
      GPIO_PORTD_DEN_R &= ~0x02;  // 5.6) disable digital I/O on PD1
      GPIO_PORTD_AMSEL_R |= 0x02; // 6.6) enable analog functionality on PD1
      break;
    case 7:                       //      Ain7 is on PD0
      GPIO_PORTD_DIR_R &= ~0x01;  // 3.7) make PD0 input
      GPIO_PORTD_AFSEL_R |= 0x01; // 4.7) enable alternate function on PD0
      GPIO_PORTD_DEN_R &= ~0x01;  // 5.7) disable digital I/O on PD0
      GPIO_PORTD_AMSEL_R |= 0x01; // 6.7) enable analog functionality on PD0
      break;
    case 8:                       //      Ain8 is on PE5
      GPIO_PORTE_DIR_R &= ~0x20;  // 3.8) make PE5 input
      GPIO_PORTE_AFSEL_R |= 0x20; // 4.8) enable alternate function on PE5
      GPIO_PORTE_DEN_R &= ~0x20;  // 5.8) disable digital I/O on PE5
      GPIO_PORTE_AMSEL_R |= 0x20; // 6.8) enable analog functionality on PE5
      break;
    case 9:                       //      Ain9 is on PE4
      GPIO_PORTE_DIR_R &= ~0x10;  // 3.9) make PE4 input
      GPIO_PORTE_AFSEL_R |= 0x10; // 4.9) enable alternate function on PE4
      GPIO_PORTE_DEN_R &= ~0x10;  // 5.9) disable digital I/O on PE4
      GPIO_PORTE_AMSEL_R |= 0x10; // 6.9) enable analog functionality on PE4
      break;
    case 10:                      //       Ain10 is on PB4
      GPIO_PORTB_DIR_R &= ~0x10;  // 3.10) make PB4 input
      GPIO_PORTB_AFSEL_R |= 0x10; // 4.10) enable alternate function on PB4
      GPIO_PORTB_DEN_R &= ~0x10;  // 5.10) disable digital I/O on PB4
      GPIO_PORTB_AMSEL_R |= 0x10; // 6.10) enable analog functionality on PB4
      break;
    case 11:                      //       Ain11 is on PB5
      GPIO_PORTB_DIR_R &= ~0x20;  // 3.11) make PB5 input
      GPIO_PORTB_AFSEL_R |= 0x20; // 4.11) enable alternate function on PB5
      GPIO_PORTB_DEN_R &= ~0x20;  // 5.11) disable digital I/O on PB5
      GPIO_PORTB_AMSEL_R |= 0x20; // 6.11) enable analog functionality on PB5
      break;
  }
	
  DisableInterrupts();
  SYSCTL_RCGCADC_R |= 0x01;     // activate ADC0 
	while((SYSCTL_PRADC_R&0x0001) != 0x0001){};
	ADC0_PC_R = 0x01;         // configure for 125K samples/sec
	ADC0_SSPRI_R = 0x3210;    // sequencer 0 is highest, sequencer 3 is lowest
	ADC0_ACTSS_R &= ~0x08;	
				
	if (period > 0){
		SYSCTL_RCGCTIMER_R |= 0x01;   // activate timer0 
		delay = SYSCTL_RCGCTIMER_R;   // allow time to finish activating
		TIMER0_CTL_R = 0x00000000;    // disable timer0A during setup
		TIMER0_CTL_R |= 0x00000020;   // enable timer0A trigger to ADC
		TIMER0_CFG_R = 0x04;             // configure for 16-bit timer mode
		TIMER0_TAMR_R = 0x00000002;   // configure for periodic mode, default down-count settings
		TIMER0_TAPR_R = 0;            // prescale value for trigger
		// Max period is 2^16 - 1. If larger period needed, divide by two and multiply the stRepeat
		adcRepeats = 1;
		adcCount = 0;
		while (period > MAX_ADC_PERIOD){
			period >>= 1;
			adcRepeats <<= 1;
		}
		TIMER0_TAILR_R = period-1;    // start value for trigger
		TIMER0_IMR_R = 0x00000000;    // disable all interrupts
		TIMER0_CTL_R |= 0x00000001;   // enable timer0A 16-b, periodic, no interrupts
		ADC0_EMUX_R = (ADC0_EMUX_R&0xFFFF0FFF)+ 0x5000; // timer trigger event
		NVIC_PRI4_R = (NVIC_PRI4_R&0xFFFF00FF)|0x00004000; //priority 2
		NVIC_EN0_R = 1<<17;              // enable interrupt 17 in NVIC
	} else {
		ADC0_EMUX_R &= ~0xF000;         // 10) seq3 is software trigger
	}
	ADC0_SSMUX3_R = channelNum;
	ADC0_SSCTL3_R = 0x06;          // set flag and end                       
	ADC0_IM_R |= 0x08;             // enable SS3 interrupts
	ADC0_ACTSS_R |= 0x08;          // enable sample sequencer 3
	EnableInterrupts();
}

volatile uint32_t ADCvalue;
void ADC0Seq3_Handler(void){
  ADC0_ISC_R = 0x08;          // acknowledge ADC sequence 3 completion
	if (++adcCount % adcRepeats == 0){
		adcBuffer[bufferPtr++] = ADC0_SSFIFO3_R;
		if (bufferPtr == bufferSize) {
			bufferReady = 1;
			TIMER0_CTL_R = 0x00000000;
		}
	}
}

uint32_t ADC_In(){
	uint32_t result;
  ADC0_PSSI_R = 0x0008;            // 1) initiate SS3
  while((ADC0_RIS_R&0x08)==0){};   // 2) wait for conversion done
    // if you have an A0-A3 revision number, you need to add an 8 usec wait here
	result = ADC0_SSFIFO3_R&0xFFF;   // 3) read result
  ADC0_ISC_R = 0x0008;             // 4) acknowledge completion
  return result;
}

// Opens channel and returns on sample
uint32_t ADC_InChan(uint8_t channel){
	ADC_Open(channel);
	return ADC_In();
}

// Returns the channel number corresponding to the pin name
// If no channel matches, returns -1
int8_t ADC_GetChannel(char* str){
	if (strcmp(str, "pe3") == 0){
		return ADC_PE3;
	} else if (strcmp(str, "pe2") == 0){
		return ADC_PE2;
	} else if (strcmp(str, "pe1") == 0){
		return ADC_PE1;
	} else if (strcmp(str, "pe0") == 0){
		return ADC_PE0;
	} else if (strcmp(str, "pd3") == 0){
		return ADC_PD3;
	} else if (strcmp(str, "pd2") == 0){
		return ADC_PD2;
	} else if (strcmp(str, "pd1") == 0){
		return ADC_PD1;
	} else if (strcmp(str, "pd0") == 0){
		return ADC_PD0;
	} else if (strcmp(str, "pe5") == 0){
		return ADC_PE5;
	} else if (strcmp(str, "pe4") == 0){
		return ADC_PE4;
	} else if (strcmp(str, "pb4") == 0){
		return ADC_PB4;
	} else if (strcmp(str, "pb5") == 0){
		return ADC_PB5;
	} else {
		return -1;
	}
}
