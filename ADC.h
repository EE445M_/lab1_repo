// ADC.h
// Module to control the TM4C's ADC

// ADC channel pins
#define ADC_PE3 0
#define ADC_PE2 1
#define ADC_PE1 2
#define ADC_PE0 3
#define ADC_PD3 4
#define ADC_PD2 5
#define ADC_PD1 6
#define ADC_PD0 7
#define ADC_PE5 8
#define ADC_PE4 9
#define ADC_PB4 10
#define ADC_PB5 11

// Open the given ADC channel 
void ADC_Open(uint8_t channelNum);

// Read one value from the ADC
uint32_t ADC_In(void);

// Open channel and read one value from ADC
uint32_t ADC_InChan(uint8_t channel);

// Initializes the ADC on channel channelNum and samples with timer triggered interrupt
void ADC_OpenPeriodic(uint8_t channelNum, uint32_t period);


//Read multiple samples with the given frequency
void ADC_Collect(uint32_t channelNum, uint32_t freq, uint32_t *buffer, uint32_t numSamples);

// Returns the status of the sample collection
uint8_t ADC_SamplesReady(void);

// Returns the channel number corresponding to the pin name
int8_t ADC_GetChannel(char* str);
