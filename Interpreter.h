// Interpreter.h

// Reads input from UART and parses into command and arguments.
// Stores results in global variables
void Interpreter_ParseCommand(void);
void Interpreter_ListAction(char* str, int32_t value);
void Interpreter_PeriodicTime(void);
void Interpreter_PeriodicADC(void);

// Interpreter actions
uint8_t Interpreter_WriteMessage(void);
uint8_t Interpreter_SetTimer(void);
uint8_t Interpreter_StopPeriodic(void);
uint8_t Interpreter_SampleADC(void);
uint8_t Interpreter_CollectADC(void);
uint8_t Interpreter_PrintSamples(void);
